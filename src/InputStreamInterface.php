<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-io-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Io;

use Stringable;

/**
 * InputStreamInterface interface file.
 *
 * An InputStreamInterface is any class that may be passed to read data from
 * another place in any program. Classes implementing this interface represents
 * a bridge between data that comes from out of the program to the program
 * itself.
 *
 * @author Anastaszor
 */
interface InputStreamInterface extends Stringable
{
	
	/**
	 * Reads up to asked number of bytes from the stream. If the end is reached
	 * and no more bytes are to be read, this returns null.
	 *
	 * @param integer $numberOfBytes
	 * @return ?string
	 */
	public function read(int $numberOfBytes) : ?string;
	
	/**
	 * Gets the next line from the stream. This returns null if there is no
	 * more lines to read.
	 *
	 * @return ?string
	 */
	public function readline() : ?string;
	
}
