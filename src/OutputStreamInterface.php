<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-io-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Io;

use Iterator;
use Stringable;

/**
 * OutputStreamInterface interface file.
 *
 * An OutputStreamInterface is any class that may be passed to write data to
 * another place in any program. Classes implementing this interface represents
 * a bridge between data that goes out of the program to a receiver container.
 *
 * @author Anastaszor
 */
interface OutputStreamInterface extends Stringable
{
	
	/**
	 * Writes the given bytes to the destination.
	 *
	 * @param string $bytes
	 * @return integer the number of bytes written
	 */
	public function write(string $bytes) : int;
	
	/**
	 * Writes the given bytes to the destination, and appends a newline at the
	 * end of the byte stream.
	 * 
	 * @param string $bytes
	 * @return integer the number of bytes written
	 */
	public function writeLn(string $bytes) : int;
	
	/**
	 * Writes the given lines to the destination with \n between each array
	 * entry.
	 * 
	 * @param array<integer, string> $bytes
	 * @return integer the total number of bytes written
	 */
	public function writeArray(array $bytes) : int;
	
	/**
	 * Writes the given lines to the destination with \n between each array
	 * entry, and appends a newline at the end of the byte stream.
	 * 
	 * @param array<integer, string> $bytes
	 * @return integer the total number of bytes written
	 */
	public function writeArrayLn(array $bytes) : int;
	
	/**
	 * Writes the given lines to the destination with \n between each iterator
	 * entry.
	 * 
	 * @param Iterator<string> $iterator
	 * @return integer the total number of bytes written
	 */
	public function writeIterator(Iterator $iterator) : int;
	
	/**
	 * Writes the given lines to the destination with \n between each iterator
	 * entry, and appends a newline at the end of the byte stream.
	 * 
	 * @param Iterator<string> $iterator
	 * @return integer the total number of bytes written
	 */
	public function writeIteratorLn(Iterator $iterator) : int;
	
}
